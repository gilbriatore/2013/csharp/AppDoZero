﻿using System;
using System.Drawing;
using System.Windows.Forms;

class Tela : Form
{
    private Button btnOk;

    static void Main(string[] args)
    {
        Tela tela = new Tela();
        Button btnUm = new Button();
        btnUm.Text = "Fechar";
        btnUm.Click += new EventHandler(btnUm_Click);

        tela.Controls.Add(btnUm);
        Application.Run(tela);        
    }

    static void btnUm_Click(object sender, EventArgs e)
    {
        Application.Exit();
    }

    public Tela()
    {
        InitializeComponent();
    }

    private void InitializeComponent()
    {
        this.btnOk = new System.Windows.Forms.Button();
        this.SuspendLayout();
        // 
        // btnOk
        // 
        this.btnOk.Location = new System.Drawing.Point(104, 113);
        this.btnOk.Name = "btnOk";
        this.btnOk.Size = new System.Drawing.Size(75, 23);
        this.btnOk.TabIndex = 0;
        this.btnOk.Text = "OK";
        this.btnOk.UseVisualStyleBackColor = true;
        this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
        // 
        // Tela
        // 
        this.ClientSize = new System.Drawing.Size(284, 262);
        this.Controls.Add(this.btnOk);
        this.Name = "Tela";
        this.DoubleClick += new System.EventHandler(this.Tela_DoubleClick);
        this.ResumeLayout(false);

    }

    private void btnOk_Click(object sender, EventArgs e)
    {
        MessageBox.Show("Botão OK foi clicado!");
    }

    private void Tela_DoubleClick(object sender, EventArgs e)
    {
        Console.WriteLine("Tela_DoubleClick");
    }
    
}